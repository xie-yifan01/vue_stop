import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Home from '../views/Home.vue'
import Welcome from '../views/Welcome.vue'
import Users from '../views/user/Users.vue'

Vue.use(VueRouter)

const routes = [
  // {
  //   path: '/',
  //   name: 'Home',
  //   component: Home
  // },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
  // 默认页面进入登录组件
  { path: '/', redirect: '/Login' },
  { path: '/Login', component: Login },
  {
    path: '/home',
    component: Home,
    redirect: '/welcome',
    children: [
      { path: '/welcome', component: Welcome },
      // 用户列表组件
      { path: '/users', component: Users }
    ]
  }
]

const router = new VueRouter({
  routes
})

// 挂载路由导航守卫:所有路由跳转时都会经过导航守卫函数的处理
// 一些有权限的组件需要经过验证
// 导航守卫只能判断token是否存在，不能判断token是否合法
router.beforeEach((to, from, next) => {
  // to将要访问的路径
  // form代表从那个路径跳转过来
  // next是一个函数，表示放行
  // next()放行   next("/login")强制跳转到longin页面
  if (to.path === '/login') {
    next()
  } else {
    const toKenStr = sessionStorage.getItem('token')
    if (!toKenStr) {
      return next('/login')
    } else {
      next()
    }
  }
})

export default router
